import requests
from scrapy.selector import HtmlXPathSelector
import pynotify
from time import sleep

def sendmessage(title, message):
 pynotify.init("Test")
 notice = pynotify.Notification(title, message)
 notice.show()
 return

url = "https://cricket.yahoo.com/#live"
while True:
 r = requests.get(url)
 while r.status_code is not 200:
 r = requests.get(url)

response = r.text
xxs = HtmlXPathSelector(text= response)
news = '\n'.join(xxs.select('//div[@class="score"]//a/text()').extract())
news = news + '\n'.join(xxs.select('//div[@class="score"]//span/text()').extract()) + ' '
news = news + '\n'.join(xxs.select('//div[@class="prev-score"]//text()').extract())
l=len(news)-2
sendmessage("IPL", news[:l])
sleep(5)