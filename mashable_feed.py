import requests
from scrapy.selector import HtmlXPathSelector
import pynotify
from time import sleep

def sendmessage(title, message):
    pynotify.init("Test")
    notice = pynotify.Notification(title, message)
    notice.show()
    return
url = "http://mashable.com/"
while True:
    r = requests.get(url)
    while r.status_code is not 200:
            r = requests.get(url)

    response = r.text
    xxs = HtmlXPathSelector(text= response)
    news = '\n'.join(xxs.select('//div[@class="column-content"]//h1/a/text()').extract()[:3])
    #print news
    sendmessage("Mashable Latest Feed", news)
    sleep(900)